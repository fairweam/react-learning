import '../css/style.scss'
import Clock from 'react-icons/lib/fa/clock-o'
import Calendar from 'react-icons/lib/fa/calendar'
import MapMarker from 'react-icons/lib/fa/map-marker'
import Group from 'react-icons/lib/md/group'
import { formatDate, formatTime } from './formatDate'
import { Link } from 'react-router'

export const EventFeatured = ({featured}) => {

    return (
        <div className="col-sm-12">
            <article className="featured-event">
                <div className="row">
                    <div className="col-md-6">
                        <img src={featured.original_image_uri} alt="" />
                    </div>
                    <div className="col-md-6">
                        <Link to={'/events/' + Number(featured.id)}><h1>{featured.title}</h1></Link>
                        <ul>
                            <li><Calendar /> {formatDate(featured.start)}</li>
                            <li><Clock /> {formatTime(featured.start)} - {formatTime(featured.end)}</li>
                            {featured.location ? <li><MapMarker /> {featured.location}</li> : null}
                            {featured.calendar.display_name ? <li><Group /> {featured.calendar.display_name}</li> : null}
                        </ul>
                        {featured.description ? <p>{featured.description}</p> : null}
                        <Link to={'/events/' + Number(featured.id)} className="btn">Event Details</Link>
                    </div>
                </div>
            </article>
        </div>
    )

}
