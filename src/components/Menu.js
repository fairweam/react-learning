import { Link } from 'react-router'
import HomeIcon from 'react-icons/lib/fa/home'
import AddIcon from 'react-icons/lib/fa/calendar-plus-o'

export const Menu = () =>

        <nav className="menu">
            <Link to="/" activeClassName="active"><HomeIcon /></Link>
            <Link to="/page" activeClassName="active">Page</Link>
            <Link to="/events" activeClassName="active">Events</Link>
        </nav>
