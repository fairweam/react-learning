import '../css/style.scss'
import { EventRow } from './EventRow'
import { Sidebar } from './Sidebar'
import { Link } from 'react-router'

export const EventList = ({events}) => {

    return (

        <div className="col-sm-8">
            <section className="event-list">
                {events.map((event, i) =>
                    <EventRow key={i}
                              id = {event.id}
                              title = {event.title}
                              description = {event.description}
                              location = {event.location}
                              image_uri = {event.image_uri}
                              start = {event.start}
                              end = {event.end}
                              display_name = {event.calendar.display_name}
                    />
                )}
            </section>
        </div>

    )
}
