import '../css/style.scss'
import { Link } from 'react-router'

export const Home = () => {

    return (

        <div className="row">
            <div className="col-sm-12">
                <section className="home">
                    <h1>Home</h1>
                </section>
            </div>
        </div>

    )
}
