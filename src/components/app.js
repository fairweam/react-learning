import { Component } from 'react'
import { Home } from './Home'
import { AddForm } from './AddForm'
import { EventList } from './EventList'
import { EventSingle } from './EventSingle'
import { EventFeatured } from './EventFeatured'
import { Sidebar } from './Sidebar'
import { Menu } from './Menu'
import axios from 'axios'

export class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            featuredData: [],
            currentData: null, type: 'all', cat: null, page: '1', results: '20', start: null,
            catData: []
        };

        this.changeCategory = this.changeCategory.bind(this);
    }

    //events endpoints
    getEventsApi() {

        const {type, cat, page, results, start } = this.state;
        const host = 'https://appbrewery.uwm.edu/api/v3.1.1/events';

        // does endpoint have value?
        const endpointCheck = (valEndpoint) => {
            if (valEndpoint === null){
                return '';
            } else if (valEndpoint === undefined) {
                return '';
            } else {
                return valEndpoint + '/';
            }
        }

        // remove trailing slash from endpoint
        const stripTrailingSlash = (apiURLEnd) => {
            return apiURLEnd.replace(/\/$/, "");
        }

        return stripTrailingSlash(host + '/' + endpointCheck(type) + endpointCheck(cat) + endpointCheck(page) + endpointCheck(results) + endpointCheck(start));

    }

    //category list endpoint
    getCategoryListApi() {
        const catHost = 'https://appbrewery.uwm.edu/api/v3.1.1/events/category-list';
        return catHost;
    }

    //change endpoint to category list
    changeCategory(categoryId) {
        const type = 'category';
        const cat = categoryId.currCatID;

        console.log(type + '/' + cat);
        console.log(cat);

        this.catRender(cat);
    }

    catRender(cat) {
        var _this = this;
        var catUrl = 'https://appbrewery.uwm.edu/api/v3.1.1/events/category/' + cat + '/1/20';
        this.serverRequest =
            axios.all([
                axios.get(catUrl)
            ])
                .then(axios.spread(function(result) {

                    var mData = result.data.data.events || [];

                    _this.setState({
                        currentData: mData,
                        loading: false
                    });

                }))

                .catch(error => console.log(error));
    }

    componentDidMount() {
        var _this = this;
        this.serverRequest =
            axios.all([
                axios.get(this.getEventsApi()),
                axios.get(this.getCategoryListApi())
            ])
                .then(axios.spread(function(result, cats) {

                    var featuredData = result.data.data.featured || [];
                    var currentData = result.data.data.events || [];
                    var catData = cats.data.data.categories || [];

                    _this.setState({
                        featuredData: featuredData,
                        currentData: currentData,
                        catData: catData,
                        mounted: true
                    });

            }))

            .catch(error => console.log(error));
    }

    render() {

        var featuredEvent;
        var featuredArray = this.state.featuredData;

        if(this.state.mounted) {

            var featuredEvent;
            var featuredArray = this.state.featuredData;

            if (typeof featuredArray !== 'undefined') {
              featuredEvent = <EventFeatured featured={featuredArray} />;
            } else {
              featuredEvent = null;
            }

            return (
                <div className="app">
                    <Menu />
                    <div className="container">
                        {
                         (this.props.location.pathname === "/page") ?
                            <AddForm /> :
                         (this.props.location.pathname === "/events") ?
                            <div className="row">
                                {featuredEvent}
                                <EventList events={this.state.currentData} />
                                <Sidebar categories={this.state.catData} onCategoryClick={this.changeCategory} />
                            </div> :
                         (this.props.location.pathname === "/events/" + Number(this.props.params.eventId)) ?
                            <div className="row">
                                <EventSingle events={this.state.currentData} eventId={this.props.params.eventId} />
                                <Sidebar categories={this.state.catData} onCategoryClick={this.changeCategory} />
                            </div> :
                            <Home />
                        }
                    </div>
                </div>
            )
        } else {
            return (
                <div>loading</div>
            )
        }
    }
}
