import '../css/style.scss'
import { PropTypes } from 'react'
import { Router, Route, Link } from 'react-router'

export const Sidebar = ({categories, currCatId, onCategoryClick}) => {

    const splitCategories = categories.filter(category => Number(category.is_audience) === Number(0))
    const splitAudience = categories.filter(category => Number(category.is_audience) === Number(1))

    const clickCategory = (catId) => {

        onCategoryClick({
            currCatID: catId
        })

        catId = ''
    }

    return (
        
        <div className="col-sm-4">
            <section className="sidebar">
                <h3>Categories</h3>
                <ul>
                {splitCategories.map((category) =>
                    <li key={category.id}><Link to={'/events/category/' + Number(category.id)} onClick={() => { clickCategory(category.id)}} activeClassName="active">{category.name}</Link></li>
                )}
                </ul>
                <h3>Audiences</h3>
                <ul>
                {splitAudience.map((category) =>
                    <li key={category.id}><Link to={'/events/category/' + Number(category.id)} onClick={() => { clickCategory(category.id)}} activeClassName="active">{category.name}</Link></li>
                )}
                </ul>
            </section>
        </div>

    )
}
