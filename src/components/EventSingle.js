import '../css/style.scss'
import { formatDate, formatTime } from './formatDate'
import { PropTypes } from 'react'
import { Router, Route, Link } from 'react-router'

export const EventSingle = ({events, eventId}) => {

        const eventFilter = events.filter(event => Number(event.id) === Number(eventId))

        return (
            <div className="col-sm-8">
                <section className="event-single">
                    {eventFilter.map((event) =>
                        <div key={event.id}>
                            <h1>{event.title}</h1>
                            <span>{event.id}</span>
                            <p>{event.description}</p>
                            <a href={event.original_event_uri}>Go to Event</a>
                        </div>
                    )}
                </section>
            </div>
        )
}
