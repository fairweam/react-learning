export function formatDate(theDate) {

    var date, monthNames, day, monthIndex, year, strDate;

    date = new Date(theDate);
    monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    day = date.getDate();
    monthIndex = date.getMonth();
    year = date.getFullYear();
    strDate = monthNames[monthIndex] + ' ' + day + ', ' + year;

    return strDate;
}

export function formatTime(theTime) {

    var time, hours, minutes, ampm, strTime;

    time = new Date(theTime);
    hours = time.getHours();
    minutes = time.getMinutes();
    ampm = hours >= 12 ? 'pm' : 'am';

    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    strTime = hours + ':' + minutes + ' ' + ampm;

    return strTime;
}
