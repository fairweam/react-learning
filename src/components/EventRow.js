import '../css/style.scss'
import Clock from 'react-icons/lib/fa/clock-o'
import Calendar from 'react-icons/lib/fa/calendar'
import MapMarker from 'react-icons/lib/fa/map-marker'
import Group from 'react-icons/lib/md/group'
import { formatDate, formatTime } from './formatDate'
import { Link } from 'react-router'

export const EventRow = ({title, id, description, location, image_uri, start, end, display_name, cat, calendar}) => (
        <article>
            <Link to={'/events/' + Number(id)}><h1>{title}</h1></Link>
            <span>{id}</span>
            <ul>
                <li>{cat}</li>
                <li><Calendar /> {formatDate(start)}</li>
                <li><Clock /> {formatTime(start)} - {formatTime(end)}</li>
                {location ? <li><MapMarker /> {location}</li> : null}
                {display_name ? <li><Group /> {display_name}</li> : null}
            </ul>
            {description ? <p>{description}</p> : null}
        </article>
)
